import numpy as np
import matplotlib.pyplot as plt

class Bezier:
    def __init__(self, numPoints):
        self.setPointsPerCurve(numPoints)
        
    def setPointsPerCurve(self, numPoints):
        self.ppc = numPoints # Points per curve
        self.t = self.generateTVector()
        self.tComplement = np.subtract(np.ones(self.ppc), self.t)

    def generateTVector(self):
        return np.linspace(0.0, 1.0, self.ppc)

    def generateCurve(self, controlPoints): # controlPoints is a list of NumPy coordinates (in any dimension)
        if len(controlPoints) <= 0:
            return

        curveDimension = len(controlPoints[0])
        controlPoints = [np.tile(point, (self.ppc, 1)) for point in controlPoints]
        t = np.tile(self.t, (curveDimension, 1)).transpose()
        tComplement = np.tile(self.tComplement, (curveDimension, 1)).transpose()

        bezierPoints = controlPoints

        while len(bezierPoints) > 1:
            newBezierPoints = []
            for index, point in enumerate(bezierPoints):
                if (index < len(bezierPoints) - 1):
                    newBezierPoints.append(np.multiply(tComplement, bezierPoints[index]) + np.multiply(t, bezierPoints[index+1]))

            bezierPoints = newBezierPoints

        return bezierPoints[0]

    def plot(self, points):
        xValues = [point[0] for point in points]
        yValues = [point[1] for point in points]

        fig, ax = plt.subplots()
        scat = ax.scatter(xValues, yValues)

        plt.show()


if __name__ == "__main__":
    x = Bezier(101)
    points = x.generateCurve([np.array([1,0]), np.array([0,0]), np.array([0,1]), np.array([1,1])])
    print(points)
    # x.plot(points)
