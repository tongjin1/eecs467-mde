import numpy as np
import matplotlib.pyplot as plt
# !pip install sympy
# from sympy.solvers import solve
# from sympy import Symbol
import math
from math import sin, cos, pi
from IPython import display
import csv
from whiteboard import Whiteboard
import time
import sys
sys.path.insert(0,"/home/student/eecs467-mde/eecs467-mde")
sys.path.insert(0,"/home/student/eecs467-mde/eecs467-mde/audio/model")
from test_micro import audio_extract

def get_EoE_transform( t ):
  t1 = t[0] * pi / 180
  t2 = t[1] * pi / 180
  t3 = t[2] * pi / 180
  joint1 = [0,0]
  joint2 = [200*cos(t1+pi/2) + 50*cos(t1+pi/2+pi/2) , 200*sin(t1+pi/2) + 50*sin(t1+pi/2+pi/2)]
  joint3 = [200*cos(t1+pi/2) + 50*cos(t1+pi/2+pi/2) + 200*cos(t1+pi/2+pi/2-t2) , 200*sin(t1+pi/2) + 50*sin(t1+pi/2+pi/2) + 200*sin(t1+pi/2+pi/2-t2)]
  joint4 = [200*cos(t1+pi/2) + 50*cos(t1+pi/2+pi/2) + 200*cos(t1+pi/2+pi/2-t2) + 174.15*cos(t1+pi/2+pi/2-t2-t3) , 200*sin(t1+pi/2) + 50*sin(t1+pi/2+pi/2) + 200*sin(t1+pi/2+pi/2-t2) + 174.15*sin(t1+pi/2+pi/2-t2-t3)]
  return [joint1, joint2, joint3, joint4]

def DrawArm(t1, t2, t3 , plot_option=False):
  joints = get_EoE_transform( [t1,t2,t3] )
  t1 = t1 * pi / 180
  t2 = t2 * pi / 180
  t3 = t3 * pi / 180
  joint1_sub = [200*cos(t1+pi/2) , 200*sin(t1+pi/2)]
  joint1 = joints[0]
  joint2 = joints[1]
  joint3 = joints[2]
  joint4 = joints[3]
  # plt.figure()
  if ( plot_option ):
    plt.plot( [joint1[0],joint1_sub[0],joint2[0],joint3[0],joint4[0]] , [joint1[1],joint1_sub[1],joint2[1],joint3[1],joint4[1]] )
    plt.scatter( [joint1[0],joint2[0],joint3[0],joint4[0]] , [joint1[1],joint2[1],joint3[1],joint4[1]], s=20, c='r' )
  return joint4 # eof

def get_translation_jacobian( theta ):
    nq = 3
    J = np.zeros((2, nq))
    joints = get_EoE_transform( theta )
    for i in range(nq):
        if ( i == 0 ):
          joint_pos_vs_eoe = [-(joints[i][1]-joints[3][1]),(joints[i][0]-joints[3][0])]
        else:
          joint_pos_vs_eoe = [(joints[i][1]-joints[3][1]),-(joints[i][0]-joints[3][0])]
        J[:,i] = np.array( joint_pos_vs_eoe )
    return J

def get_jacobian_pinv(J):
    J_pinv = []
    damp_lambda = math.sqrt( 1e-3 )
    tmp = np.eye(2)
    J_pinv = J.T@np.linalg.inv(J@J.T+tmp)
    return J_pinv

def DrawOneMoment( q , target ):
  plt.cla()
  plt.axis([-350,150,0,300])
  DrawArm( q[0] , q[1] , q[2] , True )
  plt.scatter([target[0]],[target[1]], s=50, c='g')
  display.clear_output(wait=True)
  plt.pause(1e-6)

def fixContraints(q):
  deg90 = 90
  for i in range(3):
    if ( q[i] > deg90*0.9 ):
      q[i] = deg90*0.9
    if ( q[i] < -deg90*0.9 ):
      q[i] = -deg90*0.9
  return q

def ik( target , pastq ):
    q = pastq
    error_eps = 1e0 # Ending Condition, reducing it for a higher resolution. 
    alpha = 5e1 # Calculate speed
    error_min = 999
    while ( True ):
        joints = get_EoE_transform( q )
        x_cur = np.array([joints[3][0],joints[3][1]])
        dx = np.array(target - x_cur , dtype=float)
        # print ( dx , type(dx) )
        error = np.linalg.norm(dx)
        error_min = min( error_min , error )
        # print ( error_min )
        # print ( "q" , q )
        # print ( "j" , joints )DrawOneMoment( q , target )
        
        if ( error < error_eps ):
            break
        J = get_translation_jacobian(q)
        Jpinv = get_jacobian_pinv(J)
        dq = Jpinv @ dx
        dq = dq * 1

        # print ( "J" , J)
        # print ( "dx" , dx )
        # print ( "dq" , dq )

        if ( np.linalg.norm(dq) > alpha):
            dq = alpha * dq / np.linalg.norm(dq)
        q = q - dq
        # TODO: Add joint contraints
        q = fixContraints(q)

    return q

def extract_points( points, scaleFactor = 4):
  extracted_points = []
  previous_point = points[0,:]
  for ind,point in enumerate(points):
    # print(ind)
    # print(point)
    if np.all(point == np.array([-1000,-1000])):
        extracted_points.append(point)
    # elif np.linalg.norm((point-previous_point),2) > 4:
    #   extracted_points.append(point)
    elif ind % scaleFactor == 0:
      extracted_points.append(point)
    else:
      pass
    previous_point = point
  extracted_points = np.array(extracted_points)
  return extracted_points

def main():
    while (True):
      # text_result = audio_extract()
      # print(text_result)
      confirm = input("Press confirm the text with 'confirm'\n")
      if confirm != 'confirm':
        continue
      wtb = Whiteboard("internal",100, 500, 100)
      wtb.offsetX = -500
      wtb.offsetY = 150
      wtb.offsetAdditionX = 100
      wtb.offsetAdditionY = 100
      points = wtb.write(text_result)
      points = wtb.points2array(points)
      # [-350, 150]
      # [50,  150]
      # points[:,0] = points[:,0]*40-300 + ind*40
      # points[:,1] = points[:,1]*60+ 50
      points[points < -1000] = -1000
      # print(points)
      points = extract_points(points,scaleFactor= 40)
      # print(points)
      pastq = np.array( [0,0,0] )
      # points = [[-300,100+i] for i in range(0,200,5)]
      csv_file = open('/home/student/eecs467-mde/eecs467-mde/armlab/joint_pos.csv','a')
      csv_file_writer = csv.writer(csv_file)
      csv_file_writer.writerow(np.array([-1000,-1000,-1000]))
      for p in points:
        target = np.array ( p )
        # print(target)
        if ( np.all(target == np.array([-1000,-1000])) ):
          q = np.array([-1000,-1000,-1000])
        else:
          q = ik( target , pastq  )
          DrawOneMoment( q , target )
          pastq = q
        csv_file_writer.writerow(q)
        csv_file.flush()
        # time.sleep(0.1)
        # time.sleep(0.2)

if __name__ == '__main__':
    main()
