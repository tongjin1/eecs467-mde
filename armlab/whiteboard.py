import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from letter_breaker import LetterBreaker

class Whiteboard:
    def __init__(self, font, fontsize, width, height):
        self.fontsize = fontsize
        self.width = width
        self.height = height
        self.offsetX = 0
        self.offsetY = 0
        self.offsetAdditionX = 1
        self.offsetAdditionY = 4
        self.letterBreaker = LetterBreaker(font, fontsize)

    def write(self, text):
        points = []

        for letter in text:
            # If going to bee writing off the edge, move to the next line
            if (self.offsetX + self.letterBreaker.width(letter)) >= self.width:
                self.offsetY += self.letterBreaker.lineGap
                self.offsetX = 0

            points.extend(self.letterBreaker.convert(letter, np.array([self.offsetX, self.offsetY])))

            self.offsetX += self.letterBreaker.width(letter)

        return points

    def plot(self, points):
        xValues = [point[0] for continuousLines in points for point in continuousLines]
        yValues = [point[1] for continuousLines in points for point in continuousLines]

        fig, ax = plt.subplots()
        scat = ax.scatter(xValues, yValues)

        plt.xlim(-1, self.width + 1)
        plt.ylim(-1, self.height + 1)

        plt.show()

    def points2array(self,points):
        xValues = []
        yValues = []
        for continuousLines in points:
            for point in continuousLines:
                xValues.append(point[0])
                yValues.append(point[1])
            xValues.append(-100)
            yValues.append(-100)
        return np.hstack( (np.array(xValues).reshape(len(xValues),1) , np.array(yValues).reshape(len(yValues),1)  ) )

    def visualize(self, points):
        xValues = [point[0] for continuousLines in points for point in continuousLines]
        yValues = [point[1] for continuousLines in points for point in continuousLines]

        x = []
        y = []

        fig, ax = plt.subplots()
        scat = ax.scatter(x, y)

        plt.xlim(-200, self.width + 1)
        plt.ylim(-500, self.height + 1)

        def updatePlot(i):
            x.append(xValues[i])
            y.append(yValues[i])
            scat.set_offsets(np.c_[x,y])
            return scat,

        ani = animation.FuncAnimation(fig, updatePlot, frames=len(xValues), interval=1)

        plt.show()

if __name__ == "__main__":
    x = Whiteboard("machtssr-gm", 1, 3, 2)
    x.plot(x.write("a b c d e f 1 2 3"))
    # for index in range(26):
    #     x.visualize(x.write(chr(ord("a") + index)))
