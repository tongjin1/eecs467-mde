"""!
The state machine that implements the logic.
"""
from math import pi
from PyQt4.QtCore import (QThread, Qt, pyqtSignal, pyqtSlot, QTimer)
import time
import numpy as np
import rospy
import pandas as pd

class StateMachine():
    """!
    @brief      This class describes a state machine.

                TODO: Add states and state functions to this class to implement all of the required logic for the armlab
    """

    def __init__(self, rxarm, camera):
        """!
        @brief      Constructs a new instance.

        @param      rxarm   The rxarm
        @param      planner  The planner
        @param      camera   The camera
        """
        self.rxarm = rxarm
        self.camera = camera
        self.status_message = "State: Idle"
        self.current_state = "idle"
        self.next_state = "idle"
        self.waypoints = [
            [-np.pi/2,       -0.5,      -0.3,            0.0,       0.0]
            ]
        self.writing_text_poses =[]
        self.curr_index = -1
        # self.waypoints = [
        #     [-np.pi/2,       -0.5,      -0.3,            0.0,       0.0],
        #     [0.75*-np.pi/2,   0.5,      0.3,      0.0,       np.pi/2],
        #     [0.5*-np.pi/2,   -0.5,     -0.3,     np.pi / 2,     0.0],
        #     [0.25*-np.pi/2,   0.5,     0.3,     0.0,       np.pi/2],
        #     [0.0,             0.0,      0.0,         0.0,     0.0],
        #     [0.25*np.pi/2,   -0.5,      -0.3,      0.0,       np.pi/2],
        #     [0.5*np.pi/2,     0.5,     0.3,     np.pi / 2,     0.0],
        #     [0.75*np.pi/2,   -0.5,     -0.3,     0.0,       np.pi/2],
        #     [np.pi/2,         0.5,     0.3,      0.0,     0.0],
        #     [0.0,             0.0,     0.0,      0.0,     0.0]]

    def set_next_state(self, state):
        """!
        @brief      Sets the next state.

            This is in a different thread than run so we do nothing here and let run handle it on the next iteration.

        @param      state  a string representing the next state.
        """
        self.next_state = state

    def run(self):
        """!
        @brief      Run the logic for the next state

                    This is run in its own thread.

                    TODO: Add states and funcitons as needed.
        """
        if self.next_state == "initialize_rxarm":
            self.initialize_rxarm()

        if self.next_state == "idle":
            self.idle()

        if self.next_state == "writing":
            self.writing()

        if self.next_state == "estop":
            self.estop()

        if self.next_state == "execute":
            self.execute()

        if self.next_state == "calibrate":
            self.calibrate()

        if self.next_state == "detect":
            self.detect()

        if self.next_state == "manual":
            self.manual()


    """Functions run for each state"""
    def writing(self):
        print("WRITING STATE")
        self.status_message = "State: Writing - it's trying to write a sentence"
        # for name in self.rxarm.resp.joint_names:
        # # waist shoulder elbow wrist_angle wrist_rotate gripper
        #     print(self.rxarm.get_joint_position_pid_params(name))
        # print(self.rxarm.get_joint_position_pid_params("shoulder"))
        # print(self.rxarm.get_joint_position_pid_params("elbow"))
        # print(self.rxarm.get_joint_position_pid_params("wrist_angle"))
        self.rxarm.set_joint_position_pid_params("shoulder",[4000,0,4000])
        self.rxarm.set_joint_position_pid_params("elbow",[4000,0,4000])
        self.rxarm.set_joint_position_pid_params("wrist_angle",[2400,0,8000])
        # print(self.rxarm.get_joint_position_pid_params("shoulder"))
        # print(self.rxarm.get_joint_position_pid_params("elbow"))
        # print(self.rxarm.get_joint_position_pid_params("wrist_angle"))
        while(self.next_state == "writing"):
            try:
                data = pd.read_csv("joint_pos.csv")
                data = np.array(data)
                # print(data)
                while (self.curr_index != data.shape[0]):
                    self.curr_index +=1
                    # print(self.curr_index)
                    # print(data[self.curr_index,: ] == np.array([-1000,-1000,-1000]))
                    if np.all(data[self.curr_index,: ] == np.array([-1000,-1000,-1000])):
                        if self.curr_index != 1:
                            curr_pos = np.array([0.25,self.prev_pos[1],self.prev_pos[2],self.prev_pos[3],self.prev_pos[4]])
                            self.rxarm.set_joint_positions(curr_pos)
                            curr_vels = self.rxarm.get_velocities()
                            time.sleep(0.2)
                        if self.curr_index != data.shape[0] -1:
                            curr_pos = np.array([0.25,float(data[self.curr_index+1,0])/180 * pi,float(data[self.curr_index+1,1])/180 * pi,float(data[self.curr_index+1,2])/180 * pi,1.57])
                        # print(curr_pos)scaleFactor
                            self.rxarm.set_joint_positions(curr_pos)
                            curr_vels = self.rxarm.get_velocities()
                            time.sleep(0.2)
                    else:
                        # print(curr_index)
                        curr_pos = np.array([0,float(data[self.curr_index,0])/180 * pi,float(data[self.curr_index,1])/180 * pi,float(data[self.curr_index,2])/180 * pi,1.57])
                        # print(curr_pos)
                        self.prev_pos = curr_pos
                        # print(curr_pos)
                        self.rxarm.set_joint_positions(curr_pos)
                        curr_vels = self.rxarm.get_velocities()
                        # while (sum(curr_vels) !=0):
                        #     print(curr_vels)
                        #     curr_vels = self.rxarm.get_velocities()
                        #     time.sleep(0.05)
            except:
                print("Fail to read csv")
                pass

    def manual(self):
        """!
        @brief      Manually control the rxarm
        """
        self.status_message = "State: Manual - Use sliders to control arm"
        self.current_state = "manual"

    def idle(self):
        """!
        @brief      Do nothing
        """
        self.status_message = "State: Idle - Waiting for input"
        self.current_state = "idle"

    def estop(self):
        """!
        @brief      Emergency stop disable torque.
        """
        self.status_message = "EMERGENCY STOP - Check rxarm and restart program"
        self.current_state = "estop"
        self.rxarm.disable_torque()

    def execute(self):
        """!
        @brief      Go through all waypoints
        TODO: Implement this function to execute a waypoint plan
              Make sure you respect estop signal
        """
        self.status_message = "State: Execute - Executing motion plan"
        self.next_state = "idle"

    def calibrate(self):
        """!
        @brief      Gets the user input to perform the calibration
        """
        self.current_state = "calibrate"
        self.next_state = "idle"

        """TODO Perform camera calibration routine here"""
        self.status_message = "Calibration - Completed Calibration"

    """ TODO """
    def detect(self):
        """!
        @brief      Detect the blocks
        """
        rospy.sleep(1)

    def initialize_rxarm(self):
        """!
        @brief      Initializes the rxarm.
        """
        self.current_state = "initialize_rxarm"
        self.status_message = "RXArm Initialized!"
        if not self.rxarm.initialize():
            print('Failed to initialize the rxarm')
            self.status_message = "State: Failed to initialize the rxarm!"
            rospy.sleep(5)
        self.next_state = "idle"

class StateMachineThread(QThread):
    """!
    @brief      Runs the state machine
    """
    updateStatusMessage = pyqtSignal(str)
    
    def __init__(self, state_machine, parent=None):
        """!
        @brief      Constructs a new instance.

        @param      state_machine  The state machine
        @param      parent         The parent
        """
        QThread.__init__(self, parent=parent)
        self.sm=state_machine

    def run(self):
        """!
        @brief      Update the state machine at a set rate
        """
        while True:
            self.sm.run()
            self.updateStatusMessage.emit(self.sm.status_message)
            rospy.sleep(0.03)