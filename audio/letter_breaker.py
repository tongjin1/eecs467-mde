import copy
import numpy as np
import pandas as pd
import fontTools.ttLib.ttFont as tt
from bezier import Bezier

endpoints = {
    "a": [[[np.array([1,0.3]), True], [np.array([0.8,0.1]), True], [np.array([0.2,0.1]), True], [np.array([0,0.3]), True], [np.array([0,0.8]), True], [np.array([0.2,1]), True], [np.array([0.8,1]), True], [np.array([1,0.8]), True], [np.array([1,0]), True]]],
    "b": [[[np.array([0,2]), True], [np.array([0,0.2]), True], [np.array([0.2,0]), True], [np.array([0.8,0]), True], [np.array([1,0.2]), True], [np.array([1,0.8]), True], [np.array([0.8,1]), True], [np.array([0.2,1]), True], [np.array([0,0.8]), True]]],
    "c": [[[np.array([1,0.8]), True], [np.array([0.8,1]), True], [np.array([0.2,1]), True], [np.array([0,0.8]), True], [np.array([0,0.2]), True], [np.array([0.2,0]), True], [np.array([0.8,0]), True], [np.array([1,0.2]), True]]],
    "d": [[[np.array([1,2]), True], [np.array([1,0.2]), True], [np.array([0.8,0]), True], [np.array([0.2,0]), True], [np.array([0,0.2]), True], [np.array([0,0.8]), True], [np.array([0.2,1]), True], [np.array([0.8,1]), True], [np.array([1,0.8]), True]]],
    "e": [[[np.array([0,0.5]), True], [np.array([1,0.5]), True], [np.array([1,0.8]), True], [np.array([0.8,1]), True], [np.array([0.2,1]), True], [np.array([0,0.8]), True], [np.array([0,0.2]), True], [np.array([0.2,0]), True], [np.array([0.8,0]), True], [np.array([1,0.2]), True]]],
    "f": [[[np.array([1,2]), True], [np.array([0.7,2]), True], [np.array([0.5,1.8]), True], [np.array([0.5,0]), True]], [[np.array([0,1]), True], [np.array([1,1]), True]]],
    "g": [[[np.array([0,-0.8]), True], [np.array([0.2,-1]), True], [np.array([0.8,-1]), True], [np.array([1,-0.8]), True], [np.array([1,0.8]), True], [np.array([0.8,1]), True], [np.array([0.2,1]), True], [np.array([0,0.8]), True], [np.array([0,0.2]), True], [np.array([0.2,0]), True], [np.array([0.8,0]), True], [np.array([1,0.2]), True]]],
    "h": [[[np.array([0,2]), True], [np.array([0,0]), True], [np.array([0,0.8]), True], [np.array([0.2,1]), True], [np.array([0.8,1]), True], [np.array([1,0.8]), True], [np.array([1,0]), True]]],
    "i": [[[np.array([0,0]), True], [np.array([0,1]), True]], [[np.array([0,1.5]), True]]],
    "j": [[[np.array([0,-0.8]), True], [np.array([0.2,-1]), True], [np.array([0.8,-1]), True], [np.array([1,-0.8]), True], [np.array([1,1]), True]], [[np.array([1,1.5]), True]]],
    "k": [[[np.array([0,2]), True], [np.array([0,0]), True]], [[np.array([1,1]), True], [np.array([0,0.5]), True], [np.array([1,0]), True]]],
    "l": [[[np.array([0,2]), True], [np.array([0,0]), True]]],
    "m": [[[np.array([0,1]), True], [np.array([0,0]), True], [np.array([0,0.8]), True], [np.array([0.2,1]), True], [np.array([0.8,1]), True], [np.array([1,0.8]), True], [np.array([1,0]), True], [np.array([1,0.8]), True], [np.array([1.2,1]), True], [np.array([1.8,1]), True], [np.array([2,0.8]), True], [np.array([2,0]), True]]],
    "n": [[[np.array([0,1]), True], [np.array([0,0]), True], [np.array([0,0.8]), True], [np.array([0.2,1]), True], [np.array([0.8,1]), True], [np.array([1,0.8]), True], [np.array([1,0]), True]]],
    "o": [[[np.array([0.2,0]), True], [np.array([0,0.2]), True], [np.array([0,0.8]), True], [np.array([0.2,1]), True], [np.array([0.8,1]), True], [np.array([1,0.8]), True], [np.array([1,0.2]), True], [np.array([0.8,0]), True], [np.array([0.2,0]), True]]],
    "p": [[[np.array([0,0.2]), True], [np.array([0.2,0]), True], [np.array([0.8,0]), True], [np.array([1,0.2]), True], [np.array([1,0.8]), True], [np.array([0.8,1]), True], [np.array([0.2,1]), True], [np.array([0,0.8]), True], [np.array([0,-1]), True]]],
    "q": [[[np.array([1,0.2]), True], [np.array([0.8,0]), True], [np.array([0.2,0]), True], [np.array([0,0.2]), True], [np.array([0,0.8]), True], [np.array([0.2,1]), True], [np.array([0.8,1]), True], [np.array([1,0.8]), True], [np.array([1,-1]), True]]],
    "r": [[[np.array([0,1]), True], [np.array([0,0]), True], [np.array([0,0.8]), True], [np.array([0.2,1]), True], [np.array([0.8,1]), True], [np.array([1,0.8]), True]]],
    "s": [[[np.array([1,0.8]), True], [np.array([0.8,1]), True], [np.array([0.2,1]), True], [np.array([0,0.8]), True], [np.array([0,0.7]), True], [np.array([0.2,0.5]), True], [np.array([0.8,0.5]), True], [np.array([1,0.3]), True], [np.array([1,0.2]), True], [np.array([0.8,0]), True], [np.array([0.2,0]), True], [np.array([0,0.2]), True]]],
    "t": [[[np.array([0.5,2]), True], [np.array([0.5,0]), True]], [[np.array([0,1]), True], [np.array([1,1]), True]]],
    "u": [[[np.array([0,1]), True], [np.array([0,0.2]), True], [np.array([0.2,0]), True], [np.array([0.8,0]), True], [np.array([1,0.2]), True], [np.array([1,1]), True], [np.array([1,0]), True]]],
    "v": [[[np.array([0,1]), True], [np.array([0.5,0]), True], [np.array([1,1]), True]]],
    "w": [[[np.array([0,1]), True], [np.array([0.5,0]), True], [np.array([1,1]), True], [np.array([1.5,0]), True], [np.array([2,1]), True]]],
    "x": [[[np.array([0,1]), True], [np.array([1,0]), True]], [[np.array([0,0]), True], [np.array([1,1]), True]]],
    "y": [[[np.array([0,1]), True], [np.array([0.5,0]), True]], [[np.array([0,-1]), True], [np.array([1,1]), True]]],
    "z": [[[np.array([0,1]), True], [np.array([1,1]), True], [np.array([0,0]), True], [np.array([1,0]), True]]],
    " ": []
}

glyfFlagMasks = {
    "ON_CURVE_POINT": 0x01,
    "X_SHORT_VECTOR": 0x02,
    "Y_SHORT_VECTOR": 0x04,
    "REPEAT_FLAG": 0x08,
    "X_IS_SAME_OR_POSITIVE_X_SHORT_VECTOR": 0x10,
    "Y_IS_SAME_OR_POSITIVE_Y_SHORT_VECTOR": 0x20,
    "OVERLAP_SIMPLE": 0x40
}

class LetterBreaker:
    def __init__(self, font, fontsize):
        self.scaleFactor = 1.
        self.bezier = Bezier(int(self.scaleFactor + 1))
        self.endpoints = {}
        self.advanceWidth = {}

        if font == "internal":
            self.endpoints = copy.deepcopy(endpoints)
        else:
            self.loadttf(font)

        self.updateFontSize(fontsize)

    def updateFontSize(self, fontsize):
        self.fontsize = fontsize
        self.points = {letter: self.convertToPoints(copy.deepcopy(self.endpoints[letter])) for letter in self.endpoints}

    def width(self, letter):
        if len(letter) == 1 and letter in self.advanceWidth:
            return self.advanceWidth[self.cmap[ord(letter)]]
        else:
            return self.advanceWidth[".notdef"]

    def convert(self, letter, offset):
        if len(letter) == 1:
            points = copy.deepcopy(self.points[self.cmap[ord(letter)]])
            # points = self.readcsv(letter)

            for index, continuousLines in enumerate(points):
                points[index] = [vertex + offset for vertex in continuousLines]

            return points

    def convertToPoints(self, vertices):
        for index, continuousLines in enumerate(vertices):
            vertices[index] = self.scoreBezierCurves(vertices[index])
            # vertices[index] = self.scoreLines(vertices[index])

            vertices[index] = [vertex * self.fontsize for vertex in vertices[index]]

        return vertices

    def scoreBezierCurves(self, constVertices):
        vertices = copy.deepcopy(constVertices)
        first = vertices[0][0]

        def groupCurves(curves):
            g = [curves[0][0]]
            curves.pop(0)

            for point in curves:
                if point[1] is True:
                    g.append(point[0])
                    yield g
                    g = []
                g.append(point[0])

        curves = list(groupCurves(vertices))

        points = [first]
        for curve in curves:
            points.extend(self.bezier.generateCurve(curve)[1:])

        return points
    
    def scoreLines(self, vertices):
        # All vertices are assumed to be on the curve

        totalScoredPoints = [] #List of numpy coordinates

        for index, vertex in enumerate(vertices):
            vertices[index][0] = (vertices[index][0] * self.scaleFactor).astype(int)

        for index, vertex in enumerate(vertices):
            start = vertex[0]
            if index < len(vertices) - 1:
                end = vertices[index + 1][0]
                totalScoredPoints.extend(self.scoreLine(start, end))

        totalScoredPoints.append(vertices[-1][0])

        for index, point in enumerate(totalScoredPoints):
            totalScoredPoints[index] = totalScoredPoints[index].astype(float)
            totalScoredPoints[index][0] = point[0] / self.scaleFactor
            totalScoredPoints[index][1] = point[1] / self.scaleFactor

        return totalScoredPoints

    @staticmethod
    def scoreLine(start, end):
        # Do Breshenham's Algorithm. Note: will need to scale start and end points since this will only work with integers
        # Also does not return the endpoint
        start = np.array(start).astype(int)
        end = np.array(end).astype(int)

        dx = abs(end[0] - start[0])
        dy = abs(end[1] - start[1])
        sx = 1 if start[0] < end[0] else -1
        sy = 1 if start[1] < end[1] else -1
        err = dx - dy
        x = start[0]
        y = start[1]
        scoredPoints = []

        while x != end[0] or y != end[1]:
            scoredPoints.append(np.array([x, y]))
            e2 = 2*err
            if e2 >= -dy:
                err -= dy
                x += sx
            if e2 <= dx:
                err += dx
                y += sy

        return scoredPoints

    def readcsv(self, letter):
        if len(letter) == 1:
            directory = "letter_map/"
            filename = letter + ".csv"
            filepath = directory + filename
            df = pd.read_csv(filepath)

            data = df.to_numpy()

            points = []
            points.append([point for point in data])

            return points

    def loadttf(self, fontname):
        directory = "fonts/"
        filename = fontname + ".ttf"
        filepath = directory + filename

        fontTable = tt.TTFont(filepath)

        headTable = fontTable.get("head")
        self.unitsPerEm = getattr(headTable, "unitsPerEm")

        os2Table = fontTable.get("OS/2")
        self.lineGap = (getattr(os2Table, "sTypoAscender") - getattr(os2Table, "sTypoDescender") + getattr(os2Table, "sTypoLineGap"))/self.unitsPerEm

        hmtxTable = fontTable.get("hmtx")

        cmapTable = fontTable.get("cmap")
        mapping = getattr(cmapTable, "tables")[0]
        if mapping.isUnicode():
            self.cmap = mapping.cmap

        glyfTable = fontTable.get("glyf")
        glyfNames = fontTable.getGlyphNames()

        for glyf in glyfNames:
            glyfDataTable = glyfTable.get(glyf)
            coordinates, endpoints, flags = glyfDataTable.getCoordinates(glyfTable)

            self.endpoints[glyf] = self.generatePointInfo(coordinates, endpoints, flags)

            self.advanceWidth[glyf] = hmtxTable.__getitem__(glyf)[0]/self.unitsPerEm

    def generatePointInfo(self, coordinates, endpoints, flags):
        coordinates = [np.array(coordinate)/self.unitsPerEm for coordinate in coordinates]
        flags = [self.onCurve(byte) for byte in flags]

        rangeStart = [0] + [endpoint + 1 for endpoint in endpoints[:-1]]
        rangeEnd = endpoints

        points = [list(point) for point in zip(coordinates, flags)]

        points = [points[s:e+1] for s,e in zip(rangeStart, rangeEnd)]

        # If the last point is not on the curve, use the beginning point of the contour to complete the curve
        for continuousLine in points:
            if continuousLine[-1][1] is False:
                continuousLine.append(continuousLine[0])

        # If path has multiple off-curve points in sequence, it's compressed. Add the midpoint of these two points in between as an on-curve point
        for continuousLine in points:
            for index, point in enumerate(continuousLine):
                if (index < len(continuousLine) - 1) and ((point[1] is False) and (continuousLine[index+1][1] is False)):
                    continuousLine.insert(index+1, [np.mean([point[0], continuousLine[index+1][0]], axis=0), True])

        return points

    def onCurve(self, flagByte):
        return bool(flagByte & glyfFlagMasks["ON_CURVE_POINT"])

    def printEndpoints(self, char):
        print(self.endpoints[char])

if __name__ == "__main__":
    x = LetterBreaker("machtgth", 1)
    print(x.convert("A", np.array([0,0])))
